﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityStandardAssets.ImageEffects;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class PlayVideos : MonoBehaviour
{
    public GameObject m_videoPlayerImg;
    public VideoPlayer m_videoPlayer;
    public VideoClip[] m_videoClips;

    public AudioSource m_playVideoSfx;
    public AudioSource m_closeVideoSfx;
    public AudioSource m_detectTargetSfx;
    public AudioSource m_clickSfx;
    public AudioSource m_bgMusic;

    public BlurOptimized m_blur;
    public ContrastStretch m_contrast;

    public GameObject m_menuPanel;
    public GameObject m_cameraPanel;
    public GameObject m_introPanel;
    public GameObject m_creditsPanel;

    public GameObject m_cameraUI;

    public GameObject[] m_targets;
    public GameObject m_treeTarget;

    public Image m_scanningText;

    private bool m_isShowingTargets = false;
    private bool m_playerAtTree = false;
    public Text m_playerGPS;
    public Text m_playerisAtTree;

    private void Start()
    {   m_videoPlayerImg.SetActive(false);
        ShowMenu(false);

        m_bgMusic.Play();

        m_treeTarget.SetActive(false);
        CheckTreeLocation();
    }

    public void StartCamera()
    {
        m_clickSfx.Play();

        m_introPanel.SetActive(false);
        m_blur.enabled = false;
        m_contrast.enabled = false;

        Vuforia.VuforiaUnity.SetHint(Vuforia.VuforiaUnity.VuforiaHint.HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 5);
        Vuforia.VuforiaUnity.SetHint(Vuforia.VuforiaUnity.VuforiaHint.HINT_MAX_SIMULTANEOUS_OBJECT_TARGETS, 5);

        m_cameraPanel.SetActive(true);

        m_scanningText.color = new Color(1, 1, 1, 1);
        LeanTween.value(gameObject, FadeScanningText, 1, 0, 2).setLoopPingPong();

        // show targets
        ShowTargets(true);
    }

    public void ShowMenu(bool clickSfx)
    {
        if(clickSfx)
        {   m_clickSfx.Play();
        }

        m_menuPanel.SetActive(true);
        m_blur.enabled = true;
        m_contrast.enabled = true;

        m_cameraPanel.SetActive(false);
        m_creditsPanel.SetActive(false);

        // hide targets
        ShowTargets(false);
    }

    private void ShowTargets(bool isTrue)
    {
        m_isShowingTargets = isTrue;

        for (int i = 0; i < m_targets.Length; i++)
        {
            m_targets[i].SetActive(m_isShowingTargets);
        }

        if(m_playerAtTree)
        {   m_treeTarget.SetActive(m_isShowingTargets);
        }
    }

    public void PlayVideo(int idx)
    {
        m_playVideoSfx.Play();
        m_bgMusic.Pause();

        m_videoPlayerImg.SetActive(false);
        m_videoPlayer.clip = m_videoClips[idx];

        // Each time we reach the end, we slow down the playback by a factor of 10.
        m_videoPlayer.loopPointReached += EndReached;

        // Start playback. This means the VideoPlayer may have to prepare (reserve
        // resources, pre-load a few frames, etc.). To better control the delays
        // associated with this preparation one can use videoPlayer.Prepare() along with
        // its prepareCompleted event.

        m_videoPlayer.prepareCompleted += PrepareCompleted;
        m_videoPlayer.Prepare();
        
        ShowTargets(false);

        //Invoke("DelayShowVideo", 0.5f);
    }

    public void ShowCredits()
    {
        m_clickSfx.Play();

        m_menuPanel.SetActive(false);
        m_creditsPanel.SetActive(true);
    }

    public void ShowIntro()
    {
        m_clickSfx.Play();

        m_menuPanel.SetActive(false);
        m_introPanel.SetActive(true);
    }

    public void HideIntro()
    {   
        m_introPanel.SetActive(false);
    }

    // make sure video is ready before showing
    void PrepareCompleted(UnityEngine.Video.VideoPlayer vp)
    {
        Debug.Log("Play Video");
        m_videoPlayer.Play();
        m_videoPlayerImg.SetActive(true);

        m_cameraUI.SetActive(false);
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {   CloseVideo();
    }

    public void CloseVideo()
    {
        m_closeVideoSfx.Play();
        m_bgMusic.volume = 0;
        m_bgMusic.Play();
        LeanTween.value(gameObject, FadeMusic, 0, 1, 2);

        m_videoPlayer.Stop();
        m_videoPlayer.clip = null;
        m_videoPlayerImg.SetActive(false);

        m_cameraUI.SetActive(true);

        ShowTargets(true);
    }

    public void PlayDetectTargetSfx()
    {
        if (!m_menuPanel.activeInHierarchy && !m_videoPlayerImg.activeInHierarchy)
        {   m_detectTargetSfx.Play();
        }
    }

    public void FadeScanningText(float alpha)
    {   m_scanningText.color = new Color(1, 1, 1, alpha);
    }

    public void FadeMusic(float volume)
    {   m_bgMusic.volume = volume;
    }

    public void PlayerIsAtTree()
    {
        m_playerAtTree = !m_playerAtTree;

        if (m_playerAtTree)
        {   m_playerisAtTree.text = "Player is at Tree";
            m_treeTarget.SetActive(m_isShowingTargets);
        }   else
        {   m_playerisAtTree.text = "Player is not at Tree";
            m_treeTarget.SetActive(false);
        }
    }

    public void CheckTreeLocation()
    {
        #if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {   Permission.RequestUserPermission(Permission.FineLocation);
        }
        

        StartCoroutine(CheckGPSLocation());
        #endif
    }

    IEnumerator CheckGPSLocation()
    {
        m_playerGPS.text = "Check GPS Location";

        while (true)
        {
            // First, check if user has location service enabled
            if (Input.location.isEnabledByUser)
            {
                // Start service before querying location
                Input.location.Start();

                // Wait until service initializes
                int maxWait = 20;
                while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
                {
                    yield return new WaitForSeconds(1);
                    maxWait--;
                }

                // Service didn't initialize in 20 seconds
                if (maxWait < 1)
                {
                    m_playerGPS.text = "Timed out";
                    yield break;
                }

                // Connection has failed
                if (Input.location.status == LocationServiceStatus.Failed)
                {
                    m_playerGPS.text = "Unable to determine device location";
                    yield break;
                }
                else
                {
                    // Access granted and location value could be retrieved
                    Debug.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);

                    // tree coordinates: -37.8023926, 144.962715

                    float distance = Vector2.Distance(new Vector2(Input.location.lastData.latitude, Input.location.lastData.longitude), new Vector2(-37.802387f, 144.962753f));

                    if (distance < 0.0015f)
                    {   // detect player location
                        m_playerGPS.text = "GPS: " + Input.location.lastData.latitude + ", " + Input.location.lastData.longitude + "\n" +
                                        "Tree: -37.802387,144.962753";

                        m_playerisAtTree.text = "Player is at Tree";

                        m_playerAtTree = true;
                        m_treeTarget.SetActive(m_isShowingTargets);
                    }
                    else
                    {
                        m_playerGPS.text = "GPS: " + Input.location.lastData.latitude + ", " + Input.location.lastData.longitude + "\n" +
                                        "Tree: -37.802387,144.962753 " + "Distance: " + distance;

                        m_playerAtTree = false;
                        m_treeTarget.SetActive(false);

                        m_playerisAtTree.text = "Player is not at Tree";
                    }
                }

                // Stop service if there is no need to query location updates continuously
                //Input.location.Stop();
            }
        }
    }

    private void Update()
    {
        // Make sure user is on Android platform
        #if PLATFORM_ANDROID
        // Check if Back was pressed this frame
        if (Input.GetKeyDown(KeyCode.Escape))
        {   // Quit the application
            Application.Quit();
        }
        #endif
    }
}
